
function i_nvim {
	mkdir -pv ~/.config/oni
	ln -fns `pwd` ~/.config/nvim
	ln -fns `pwd`/tmux.conf ~/.tmux.conf
}

function i_nvim_apt {
	sudo add-apt-repository ppa:neovim-ppa/stable -y
	sudo apt-get update -y
	sudo apt-get install -y neovim
}

function i_nvim_yum {
	sudo yum install -y neovim
}

function i_nvim_dein {
	curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
	sh installer.sh ~/.cache/dein
	rm installer.sh
}

function i_nvim_python {
	pip3 install --user neovim
	curl -sL install-node.now.sh/lts | sudo bash
}

function i_nvim_settings {
	echo "alias vim=nvim" > settings
	echo "source $PATH/settings" >> ~/.bashrc
}

PS3='Please enter your choice: '
options=("Install w/ apt" "Install w/ yum" "Build from source")
select opt in "${options[@]}"
do
    case $opt in
        "Install w/ apt")
            echo "Installing w/ apt"
			i_nvim
			i_nvim_apt
			i_nvim_settings
			i_nvim_python
			i_nvim_dein
            ;;
        "Install w/ yum")
            echo "you chose choice 2"
			i_nvim
			i_nvim_yum
			i_nvim_settings
			i_nvim_python
			i_nvim_dein
            ;;
        "Build from source")
            echo "Later...."
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

