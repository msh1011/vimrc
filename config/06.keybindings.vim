nnoremap ; :
nnoremap ? :noh<CR>
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>

nnoremap <F3> :ToggleWorkspace<CR>
nnoremap <F2> :NERDTreeTabsToggle<CR>

nnoremap <C-Space> :Nuake<CR>
inoremap <C-Space> <C-\><C-n>:Nuake<CR>
tnoremap <C-Space> <C-\><C-n>:Nuake<CR>

nnoremap <F8> :Nuake<CR>make run<CR>
inoremap <F8> <C-\><C-n>:Nuake<CR>make run<CR>
tnoremap <F8> make run<CR>

nnoremap <F7> :Autoformat<CR>
inoremap <F7> :Autoformat<CR>

nnoremap <C-p> :FuzzyOpen<CR>
tnoremap <Esc> <C-\><C-n>

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

