colorscheme quantum
set termguicolors

let NERDTreeDirArrows = 1
let NERDTreeMinimalUI = 1
let NERDTreeIgnore = ['\.pyc$']

let g:workspace_autosave_untrailspaces = 0
let g:workspace_session_directory = $HOME . '/.nvim_state/'
let g:workspace_undodir = $HOME . '/.nvim_undo/'

let g:deoplete#sources#go#gocode_binary = $GOPATH . '/bin/gocode'

let g:nuake_position='top'
let g:nuake_size=0.7


let g:formatdef_cpp='"uncrustify -c ~/.uncrustify.cfg -q"'
let g:formatters_cpp = ['cpp']
let g:formatters_c = ['cpp']

autocmd VimEnter,BufNewFile,BufReadPost * silent! call HardMode()
let g:HardMode_level='wannabe'
