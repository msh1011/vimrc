if &compatible
    set nocompatible
endif

set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.cache/dein')
    call dein#begin('~/.cache/dein')
    call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

    "Plugins
    call dein#add("scrooloose/nerdtree")
    call dein#add("jistr/vim-nerdtree-tabs")

    call dein#add('neoclide/coc.nvim', { 'rev': 'release' })
    call dein#add('derekwyatt/vim-scala')

    call dein#add('fatih/vim-go')

    call dein#add('roxma/nvim-yarp')
    call dein#add('roxma/vim-hug-neovim-rpc')

    call dein#add('mhartington/oceanic-next')
    call dein#add('tyrannicaltoucan/vim-quantum')

    call dein#add('vim-airline/vim-airline')
    call dein#add('vim-airline/vim-airline-themes')

    call dein#add('thaerkh/vim-workspace')
    call dein#add('cloudhead/neovim-fuzzy')

    call dein#add('w0rp/ale')
    call dein#add('Chiel92/vim-autoformat')

    call dein#add('Lenovsky/nuake')
    call dein#add('airblade/vim-gitgutter')
    call dein#add('jiangmiao/auto-pairs')
    call dein#add('wikitopian/hardmode')

    "End

    call dein#end()
    call dein#save_state()
endif


filetype plugin indent on
syntax enable


if dein#check_install()
    call dein#install()
endif
