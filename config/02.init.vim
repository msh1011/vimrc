
set t_Co=256


set relativenumber
set number
set autoindent
set tabstop=4
set shiftwidth=4
set noexpandtab
set sts=4
set list

set splitright
set splitbelow

set mouse=a mousemodel=popup
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<

autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
autocmd VimLeavePre * NERDTreeTabsClose


autocmd BufWinEnter,WinEnter term://* startinsert
autocmd BufLeave term://* stopinsert

let g:pymode_python = 'python3'

au BufRead,BufNewFile *.sbt set filetype=scala

